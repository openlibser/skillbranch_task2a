const express = require('express');

const app = express();

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
}

app.use(allowCrossDomain);

app.get('/', (req, res) => {
  const a = isNumeric(+req.query.a) ? +req.query.a : 0;
  const b = isNumeric(+req.query.b) ? +req.query.b : 0;
  res.send( (a + b).toString() );
});

app.listen(3000, () => console.log('Server runing on 3000 port!'));
